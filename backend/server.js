const express = require("express");
const connectDB = require("./config/db");
const dotenv = require("dotenv").config();
const cors = require('cors');
const port = 5000;

// Connecter à la base de données
connectDB();

// Initialiser l'application Express
const app = express();

// Autoriser Cors pour toutes les requêtes app.use(cors());
// Configurer CORS pour autoriser uniquement les requêtes de http://localhost:5173
app.use(cors({ origin: 'http://localhost:5173' }));      


// Middleware pour analyser les requêtes JSON et URL-encoded
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Définir les routes
app.use("/post", require("./routes/post.routes"));

// Démarrer le serveur
app.listen(port, () => console.log("Le serveur a démarré au port " + port));
