const PostModel = require('../models/post.model');

module.exports.getPosts = async (req, res) => {
    try {
        const posts = await PostModel.find();
        res.status(200).json(posts);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

module.exports.setPosts = async (req, res) => {
    try {
        // Validation des entrées
        if (!req.body.message) {
            return res.status(400).json({ message: "Merci d'ajouter un message" });
        }

        if (!req.body.author) {
            return res.status(400).json({ message: "Merci d'ajouter un auteur" });
        }

        // Création du post
        const post = await PostModel.create({
            message: req.body.message,
            author: req.body.author
        });

        // Envoi de la réponse
        return res.status(200).json(post);

    } catch (error) {
        // Gestion des erreurs
        console.error(error);
        return res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

module.exports.editPost = async (req, res) => {
    try {
        const post = await PostModel.findById(req.params.id);

        if (!post) {
            return res.status(404).json({ message: "Ce post n'existe pas" });
        }

        const updatedPost = await PostModel.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true, // Pour valider les modifications selon le schéma
        });

        return res.status(200).json(updatedPost);

    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

module.exports.deletePost = async (req, res) => {
    try {
        const post = await PostModel.findById(req.params.id);

        if (!post) {
            return res.status(400).json({ message: "Ce post n'existe pas" });
        }

        await PostModel.findByIdAndDelete(req.params.id);
        return res.status(200).json({ message: `Message supprimé : ${post.message}` });

    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

module.exports.likePost = async (req, res) => {
    try {
        await PostModel.findByIdAndUpdate (
            req.params.id,
            {$addToSet: {likers: req.body.userId}},
            {new: true}
        ).then((data)=> res.status(200).send(data));
    } catch (error) {
        res.status(400).json(error);
    };
};

module.exports.dislikePost = async (req, res) => {
    try {
        await PostModel.findByIdAndUpdate (
            req.params.id,
            {$pull: {likers: req.body.userId}},
            {new: true}
        ).then((data)=> res.status(200).send(data));
    } catch (error) {
        res.status(400).json(error);
    };
};






